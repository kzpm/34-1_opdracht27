! R1

en

reload
no
conf t
h R1

ip dhcp pool kamer12
network 192.168.50.0 255.255.255.0
default-router 192.168.50.1

ip dhcp pool kamer14
network 172.30.0.0 255.255.255.0
default-router 172.30.0.1

exit

ip dhcp excluded-address 192.168.50.1 192.168.50.49
ip dhcp excluded-address 192.168.50.200 192.168.50.254

ip dhcp excluded-address 172.30.0.1 172.30.0.49
ip dhcp excluded-address 172.30.0.200 172.30.0.254

int fa0/0
ip add 172.30.0.1 255.255.255.0
no shut

int fa0/1
ip add 192.168.50.1 255.255.255.0
no shut

int eth0/0/0
ip add 10.0.0.1 255.255.255.0
no shut

end





